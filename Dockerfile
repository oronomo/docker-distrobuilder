FROM registry.gitlab.com/oronomo/docker-ubuntu:latest

RUN DEBIAN_FRONTEND=noninteractive apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y debootstrap golang-go gpg lxd squashfs-tools
RUN DEBIAN_FRONTEND=noninteractive apt upgrade -y

RUN go get -d -v github.com/lxc/distrobuilder/distrobuilder && \
    cd $HOME/go/src/github.com/lxc/distrobuilder && \
    make

RUN echo "export PATH=/root/go/bin:${PATH}" >> /root/.bashrc
ENV PATH /root/go/bin:${PATH}

CMD ["bash"]
